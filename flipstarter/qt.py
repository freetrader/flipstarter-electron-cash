##!/usr/bin/env python3
#
# Author: Calin Culianu <calin.culianu@gmail.com>
# Author: Dagur Valberg Johannsson <dagurval@pvv.ntnu.no>
# Copyright (C) 2019 Calin Culianu
# Copyright (C) 2020 Dagur Valberg Johannsson
# LICENSE: MIT
#
import os

from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

from electroncash.bitcoin import TYPE_ADDRESS
from electroncash.i18n import _
from electroncash.plugins import BasePlugin, hook
from electroncash.transaction import InputValueMissing
from electroncash.transaction import Transaction
from electroncash.util import NotEnoughFunds
from electroncash.util import print_error
from electroncash.util import PrintError
from electroncash.util import UserCancelled

from .funderutil import cancel_pledge
from .funderutil import coin_to_outpoint
from .funderutil import decode_template
from .funderutil import get_pledges
from .funderutil import memorize_pledge
from .funderutil import sats_to_bch
from .funderutil import serialize_pledge_input
from .funderutil import template_to_tx
from .walletutil import is_wallet_compatible
from .walletutil import get_wallet_address
from .known_addr import summarize_outputs


class Plugin(BasePlugin):
    def __init__(self, parent, config, name):
        super().__init__(parent, config, name)
        # list of 'Instance' objects
        self.instances = list()
        # pointer to Electrum.gui singleton
        self.gui = None
        self.config = config
        self.is_slp = False

    def shortName(self):
        return _("Flipstarter")

    def icon(self):
        from .resources import qInitResources  # lazy importing
        return QIcon(":flipstarter/resources/icon.png")

    def iconLarge(self):
        from .resources import qInitResources  # lazy importing
        return QIcon(":flipstarter/resources/icon64.png")

    def description(self):
        return _("Raise funds cooperatively")

    def thread_jobs(self):
        return list()

    def on_close(self):
        """
        BasePlugin callback called when the wallet is disabled among other things.
        """
        ct = 0
        for instance in self.instances:
            instance.close()
            ct += 1
        self.instances = list()
        self.print_error("on_close: closed %d extant instances" % (ct) )

    @hook
    def init_qt(self, qt_gui):
        """
        Hook called when a plugin is loaded (or enabled).
        """
        self.gui = qt_gui
        # We get this multiple times.  Only handle it once, if unhandled.
        if len(self.instances):
            return

        # These are per-wallet windows.
        for window in self.gui.windows:
            self.load_wallet(window.wallet, window)

    @hook
    def load_wallet(self, wallet, window):
        """
        Hook called when a wallet is loaded and a window opened for it.
        """
        self.instances.append(Instance(self, wallet, window))

    @hook
    def close_wallet(self, wallet):
        for instance in self.instances:
            if instance.wallet == wallet:
                iname = instance.diagnostic_name()
                instance.close()
                # pop it from the list of instances, python gc will remove it
                self.instances.remove(instance)
                self.print_error("removed instance:", iname)
                return


class Instance(QWidget, PrintError):
    """Encapsulates a wallet-specific instance."""

    sig_network_updated = pyqtSignal()
    sig_user_tabbed_to_us = pyqtSignal()
    sig_user_tabbed_from_us = pyqtSignal()
    sig_window_moved = pyqtSignal()
    sig_window_resized = pyqtSignal()
    sig_window_activation_changed = pyqtSignal()
    sig_window_unblocked = pyqtSignal()

    def __init__(self, plugin, wallet, window):
        super().__init__()
        self.plugin = plugin
        self.wallet = wallet
        self.window = window
        self.window.installEventFilter(self)
        self.wallet_name = os.path.split(wallet.storage.path)[1]
        self.already_warned_incompatible = False
        self.disabled = False
        self.did_register_callback = False

        wallet_compatible, compatible_err = is_wallet_compatible(wallet)
        self.incompatible = not wallet_compatible
        if compatible_err is not None:
            self.print_error("Incompatible wallet {}".format(compatible_err))
        self.incompatible_error = compatible_err

        # do stuff to setup UI...
        from .ui import Ui_Instance
        self.ui = Ui_Instance()
        self.ui.setupUi(self)

        self.ui.confirm.clicked.connect(self.on_confirm_pledge)
        self.ui.commitment.selectionChanged.connect(
            self.on_commitment_selection_change)

        self.ui.template_in.textChanged.connect(self.on_template_change)
        self.template_stylesheet_ok = self.ui.template_in.styleSheet()

        self.disable_if_incompatible()

        # Add menu items to menu button
        menu = QMenu()
        self.cancel_pledges_action = menu.addAction(_("Cancel all pledges"))
        self.show_about_action = menu.addAction(_("About Flipstarter"))
        self.ui.menu.setMenu(menu)

        self.cancel_pledges_action.triggered.connect(self.on_cancel_pledges)
        self.show_about_action.triggered.connect(self.on_show_about)

        # finally, add the UI to the wallet window
        window.tabs.addTab(self, plugin.icon(), plugin.shortName())

    def get_wallet_password(self):
        password = None
        if self.wallet.has_password():
            password = self.window.password_dialog(
                    _('Your wallet is encrypted.') + '\n' +
                    _('Please enter your password to continue.'))
            if not password:
                raise Exception(_("Password required"))
        return password

    def on_show_about(self):
        QMessageBox.about(self, self.plugin.shortName(),
            "{} — {}".format(self.plugin.shortName(), self.plugin.description()))

    def on_template_change(self):
        self.ui.commitment.setPlainText("")
        self.ui.commitment.setEnabled(False)
        payload = self.ui.template_in.toPlainText()
        if len(payload) == 0:
            self.ui.amount_lbl.setText("0 BCH")
            self.ui.pledger_alias.setText("")
            self.ui.pledger_comment.setText("")
            self.ui.template_in.setStyleSheet(self.template_stylesheet_ok)
            self.set_status("")
            return
        try:
            template = decode_template(payload, check_expired=True)
        except Exception as e:
            self.ui.amount_lbl.setText("UNKNOWN")
            self.ui.template_in.setStyleSheet("background-color: #ffcccb")
            self.set_status(str(e), is_warning = True)
            return

        self.ui.template_in.setStyleSheet(self.template_stylesheet_ok)
        self.set_status("")
        donation_bch = sats_to_bch(template["donation"]["amount"])
        self.ui.amount_lbl.setText("{} BCH".format(donation_bch))
        self.ui.pledger_alias.setText(template["data"]["alias"])
        self.ui.pledger_comment.setText(template["data"]["comment"])

    def on_commitment_selection_change(self):
        text = self.ui.commitment.toPlainText()
        if len(text) == 0:
            # Nothing to select
            return

        cursor = self.ui.commitment.textCursor()

        if cursor.isNull():
            # Not sure why cursor is sometimes null, but without
            # this check, this signal will recurse forever.
            # See issue #25
            return

        if len(text) == len(cursor.selectedText()):
            # We have full selection.
            return

        # Partial selection, force full selection
        self.ui.commitment.selectAll()

    def on_confirm_pledge(self):
        payload = self.ui.template_in.toPlainText()
        try:
            template = decode_template(payload, check_expired=True)
            output_addrs = [o["address"] for o in template["outputs"]]
            output_summary = summarize_outputs(output_addrs)
        except Exception as e:
            return self.show_error(str(e))

        donation_sats = template["donation"]["amount"]
        donation_bch_human = sats_to_bch(template["donation"]["amount"])

        pledge_question = "{} {} BCH\n\n{}".format(
            _("Do you want to pledge"),
            donation_bch_human,
            output_summary)

        if not self.window.question(pledge_question):
            return self.show_error(_("Pledge cancelled by user"))

        if not self.wallet.network:
            return self.show_error(_("Electron Cash is not connected to the network"))

        try:
            password = self.get_wallet_password()
            tx, pledge_input = self.create_tx_to_self(donation_sats, password)
        except NotEnoughFunds as _e:
            return self.show_error(_("Not enough funds in wallet"))
        except Exception as e:
            e_str = str(e)
            if len(e_str) == 0:
                e_str = "Unknown"
            return self.show_error("{}: {}".format(_("Failed to create a transaction to ourselves"), e_str))

        frozen = self.wallet.set_frozen_coin_state([pledge_input], True)

        if frozen != 1:
            return self.show_error(_("Failed to freeze pledge"))

        try:
            signed = self.sign_pledge_tx(password, template, pledge_input)
        except Exception as e:
            return self.show_error("{}: {}".format(_("Failed to sign pledge"), e))

        try:
            commitment = serialize_pledge_input(signed, self.ui.pledger_alias.text(), self.ui.pledger_comment.text())
        except Exception as e:
            return self.show_error("{}: {}".format(_("Failed to serialize pledge"), e))

        if self.wallet.network:
            ok, details = self.wallet.network.broadcast_transaction(tx)
            if not ok:
                return self.show_error("{}: {}".format(_("Failed to broadcast transaction"), details))
            self.wallet.set_label(details, _("Flipstarter pledge. To cancel, see FAQ at flipstarter.cash."))
        else:
            # Should not happen, as we already checked for this above
            # before freezing coins
            return self.show_error(_("Connection error"))

        # Remember the pledge so we can cancel it later
        memorize_pledge(self.wallet.storage, pledge_input)

        self.ui.commitment.setPlainText(commitment)
        self.ui.commitment.setEnabled(True)
        self.window.show_message(_("Your pledge is prepared. You must copy and paste the details into the campaign site to complete the pledge."))

    def sign_pledge_tx(self, password, template, pledge_input):
        tx = template_to_tx(template, pledge_input)

        # SIGHASH_ANYONECANPAY | SIGHASH_ALL is not supported by
        # electrum, hence this ugly hack.
        signed = False
        tx.__class__ = HackedTransaction
        if self.wallet.is_watching_only():
            raise Exception(_("Watch only wallet"))
        for k in self.wallet.get_keystores():
            try:
                if k.can_sign(tx):
                    k.sign_transaction(tx, password)
                    signed = True
                else:
                    self.print_error("Key store {} cannot sign {}".format(k, tx))
            except UserCancelled:
                continue

        if not signed:
            raise Exception(_("Unable to sign"))

        return tx

    def create_tx_to_self(self, amount, password):
        coins = self.wallet.get_utxos(exclude_frozen=True,
                                      exclude_slp=True)

        frozen_pledge_addr = get_wallet_address(self.wallet)

        tx = self.wallet.make_unsigned_transaction(
                inputs=coins,
                outputs=[(TYPE_ADDRESS, frozen_pledge_addr, amount)],
                config=self.plugin.config)

        self.wallet.sign_transaction(tx, password)

        # find our output pledge output
        pledge_input = None
        for i, (t, addr, v) in enumerate(tx.outputs()):
            if t != TYPE_ADDRESS:
                continue
            if addr != frozen_pledge_addr:
                continue
            if v != amount:
                continue

            # match
            pledge_input = {
                'address': addr,
                'type': 'p2pkh',
                'prevout_hash': tx.txid(),
                'prevout_n': i,
                'sequence': 0xffffffff,
                'value': amount
            }
            self.wallet.add_input_sig_info(pledge_input, addr)
            break

        if pledge_input is None or pledge_input['address'] is None:
            raise Exception(_("Pledge output missing"))

        return tx, pledge_input

    def show_error(self, msg):
        self.window.show_error(msg=msg, title=_("Error: ") + self.plugin.shortName(), parent=self.window)

    def on_cancel_pledges(self):
        """Cancel all known pledge coins"""
        actual_coins = self.wallet.get_utxos(exclude_frozen=False,
                                             exclude_slp=True)
        existing_pledges = get_pledges(self.wallet.storage, actual_coins)

        # notify and stop if no pledges to cancel
        if not existing_pledges:
            self.window.show_message(_("No known pledges to cancel - if there are any, please unfreeze and spend them manually in 'Coins' tab"))
            return

        # confirm before canceling
        if not self.window.question(
                "{} ({})".format(_("Do you want to cancel all current pledges?"),
                                 len(existing_pledges))):
            return

        # do the cancellations
        success_count = 0
        failed_coins = list()
        try:
            password = self.get_wallet_password()
        except Exception as e:
            self.window.show_message(_("Failed to cancel pledges: {}". format(e)))
            return

        for c in existing_pledges:
            try:
                cancel_pledge(self.wallet, password, self.plugin.config, c)
                success_count += 1
            except Exception as e:
                self.print_error("Failed to cancel pledge: {}".format(repr(e)))
                failed_coins.append(c)

        # notify the user how everything went
        msg = "{}: {}".format(_("Cancelled pledges"), success_count)
        if failed_coins:
            msg += "\n{}\n{}".format(_("Failed to cancel some pledges. Please unfreeze and spend them in 'Coins' tab"),
                                     "\n".join(coin_to_outpoint(c) for c in failed_coins))
        self.window.show_message(msg)

    def on_network(self, event, *args):
        # network thread
        if event == 'updated' and (not args or args[0] is self.wallet):
            self.sig_network_updated.emit()  # this passes the call to the gui thread
        elif event == 'verified':  # grr.. old api sucks
            self.sig_network_updated.emit()  # this passes the call to the gui thread
        elif event in ('wallet_updated', 'verified2') and args[0] is self.wallet:
            self.sig_network_updated.emit()  # this passes the call to the gui thread
        elif event == 'blockchain_updated':
            self.sig_network_updated.emit()  # this passes the call to the gui thread

    def on_user_tabbed_to_us(self):
        warn_user = False
        try:
            if self.incompatible and not self.already_warned_incompatible:
                self.already_warned_incompatible = True
                warn_user = True
        except AttributeError:
            # Exception happens because I don't think all
            # wallets have the is_watching_only method
            pass

        if warn_user:
            if self.is_slp:
                msg = _("This is an incompatible wallet type.") + "\n\n" \
                    + _("SLP token wallets are not supported, as a safety feature.")
            else:
                msg = _("This is an incompatible wallet type.") + "\n\n" \
                    + _("The plugin only supports imported private key or standard spending wallets.")
            self.window.show_error(msg=msg,
                                   title="{} - {}".format(self.plugin.shortName(), _("Incompatible Wallet")),
                                   parent=self.window)

    def disable_if_incompatible(self):
        if not self.incompatible:
            return

        self.disabled = True
        warning = "{} {}".format(
            _("The plugin is disabled. This wallet is incompatible:"),
            self.incompatible_error)

        self.print_error(warning)
        self.set_status(warning, is_warning = True)

        gbs = [
            self.ui.confirm,
            self.ui.menu,
            self.ui.pledger_alias,
            self.ui.pledger_comment,
            self.ui.template_in,
        ]
        for gb in gbs:
            gb.setEnabled(False)  # disable all controls

    def event(self, event):
        """overrides QObject: a hack used to detect when the prefs
           screen was closed or when our tab comes to foreground."""
        if event.type() in (QEvent.WindowUnblocked, QEvent.ShowToParent) and self.wallet:
            if event.type() == QEvent.ShowToParent:
                self.sig_user_tabbed_to_us.emit()
            else:
                self.sig_window_unblocked.emit()

            # if window unblocked, maybe user changed prefs.
            # inform our managers to refresh() as maybe base_unit changed, etc.
            self.refresh_all()

        elif event.type() in (QEvent.HideToParent,):
            # user left our UI. Tell interested code about
            # this (mainly the PopupLabel cares)
            self.sig_user_tabbed_from_us.emit()

        # Make real QWidget implementation actually handle the event
        return super().event(event)

    def eventFilter(self, window, event):
        """Spies on events to parent window to figure out
           when the user moved or resized the window, and announces
           that fact via signals."""
        if window == self.window:
            if event.type() == QEvent.Move:
                self.sig_window_moved.emit()
            elif event.type() == QEvent.Resize:
                self.sig_window_resized.emit()
            elif event.type() == QEvent.ActivationChange:
                self.sig_window_activation_changed.emit()
        return super().eventFilter(window, event)

    def refresh_all(self):
        self.print_error("refresh_all")

    def wallet_has_password(self):
        try:
            return self.wallet.has_password()
        except AttributeError:  # happens on watching-only wallets which don't have the requisite methods
            return False

    # Uncomment to test object lifetime and make sure Qt is deleting us.
    #def __del__(self):
    #    print("**** __del__ ****")

    # called by self.plugin on wallet close - deallocate all resources and die.
    def close(self):
        self.print_error("Close called on an Instance")
        if self.window:
            self.window.removeEventFilter(self)
            ix = self.window.tabs.indexOf(self)
            if ix > -1:
                self.window.tabs.removeTab(ix)
                # since qt doesn't delete us, we need to explicitly
                # delete ourselves, otherwise the QWidget lives around
                # forever in memory
                self.deleteLater()
        self.disabled = True

        # trigger object cleanup sooner rather than later!
        self.window, self.plugin, self.wallet_name = (None,) * 3

    # overrides PrintError super
    def diagnostic_name(self):
        if hasattr(self.wallet, "diagnostic_name") and callable(self.wallet.diagnostic_name):
            return self.wallet.diagnostic_name() + ".flipstarter"
        return super().diagnostic_name() + ".flipstarter"

    def set_status(self, text, is_warning = False):
        self.ui.status.setText(text)
        if is_warning:
            self.ui.status.setStyleSheet("color: red")
        else:
            self.ui.status.setStyleSheet("")

#####

# Hacks to sign with SIGHASH_ANYONECANPAY from jcramer
# https://github.com/simpleledger/Electron-Cash-SLP
class HackedTransaction(Transaction):
    def serialize_preimage(self, i, nHashType=0x00000041, use_cache = False):
        from electroncash.bitcoin import int_to_hex, var_int, bh2u
        if not (nHashType & 0xff) in [0xc1]:
            raise ValueError("wrong hashtype for this hack")

        nVersion = int_to_hex(self.version, 4)
        nHashType = int_to_hex(nHashType, 4)
        nLocktime = int_to_hex(self.locktime, 4)

        txin = self.inputs()[i]
        outpoint = self.serialize_outpoint(txin)
        preimage_script = self.get_preimage_script(txin)
        scriptCode = var_int(len(preimage_script) // 2) + preimage_script
        try:
            amount = int_to_hex(txin['value'], 8)
        except KeyError:
            raise InputValueMissing
        nSequence = int_to_hex(txin['sequence'], 4)

        hashPrevouts, hashSequence, hashOutputs = self.calc_common_sighash(use_cache=use_cache)

        hashPrevouts = "0000000000000000000000000000000000000000000000000000000000000000"
        hashSequence = "0000000000000000000000000000000000000000000000000000000000000000"

        preimage = nVersion + hashPrevouts + hashSequence + outpoint + scriptCode + amount + nSequence + bh2u(hashOutputs) + nLocktime + nHashType
        return preimage

    def sign(self, keypairs, *, use_cache=False):
        for i, txin in enumerate(self.inputs()):
            pubkeys, x_pubkeys = self.get_sorted_pubkeys(txin)
            for j, (pubkey, x_pubkey) in enumerate(zip(pubkeys, x_pubkeys)):
                if self.is_txin_complete(txin):
                    # txin is complete
                    break
                if pubkey in keypairs:
                    _pubkey = pubkey
                    kname = 'pubkey'
                elif x_pubkey in keypairs:
                    _pubkey = x_pubkey
                    kname = 'x_pubkey'
                else:
                    continue
                print_error(f"adding signature for input#{i} sig#{j}; {kname}: {_pubkey} schnorr: {self._sign_schnorr}")
                sec, compressed = keypairs.get(_pubkey)
                self._sign_txin(i, j, sec, compressed)
        print_error("is_complete", self.is_complete())
        self.raw = self.serialize()

    def _sign_txin(self, i, j, sec, compressed, *, use_cache=False):
        from electroncash.bitcoin import public_key_from_private_key, Hash, bfh, bh2u
        '''Note: precondition is self._inputs is valid (ie: tx is already deserialized)'''
        pubkey = public_key_from_private_key(sec, compressed)
        # add signature
        nHashType = 0x000000c1
        pre_hash = Hash(bfh(self.serialize_preimage(i, nHashType)))
        if self._sign_schnorr:
            sig = self._schnorr_sign(pubkey, sec, pre_hash)
        else:
            sig = self._ecdsa_sign(sec, pre_hash)
        reason = []
        if not self.verify_signature(bfh(pubkey), sig, pre_hash, reason=reason):
            print_error(f"Signature verification failed for input#{i} sig#{j}, reason: {str(reason)}")
            return None
        txin = self._inputs[i]
        txin['signatures'][j] = bh2u(sig + bytes((nHashType & 0xff,)))
        txin['pubkeys'][j] = pubkey # needed for fd keys
        return txin
